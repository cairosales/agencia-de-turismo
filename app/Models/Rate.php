<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Rate extends Model
{
    use HasFactory;
    protected $fillable = [
        'value','active','coin','room','hotel'
    ];


    public function setActiveAttribute($value)
    {
        $this->attributes['active'] = (bool) $value;
    }
    public function getActiveAttribute()
    {
        if($this->attributes['active'] !=  null){
            return (bool) $this->attributes['active'];
        }
    }



    public function coin(){
        return $this->belongsTo(Coin::class,'coin','id');
    }
    public function hotel(){
        return $this->belongsTo(Hotel::class,'hotel','id');
    }
    public function room(){
        return $this->belongsTo(Room::class,'room','id');
    }
}
