<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Room extends Model
{
    use HasFactory;
    protected $fillable = [
        'name','active'
    ];


    public function setActiveAttribute($value)
    {
        $this->attributes['active'] = (bool) $value;
    }
    public function getActiveAttribute()
    {
        if($this->attributes['active'] !=  null){
            return (bool) $this->attributes['active'];
        }
    }
}
