<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Hotel extends Model
{
    use HasFactory;

    protected $fillable = [
        'name','active','address','neighborhood','number','zipcode','stars'
    ];


    public function setActiveAttribute($value)
    {
        $this->attributes['active'] = (bool) $value;
    }
    public function getActiveAttribute()
    {
        if($this->attributes['active'] !=  null){
            return (bool) $this->attributes['active'];
        }
    }
}
