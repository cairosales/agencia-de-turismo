<?php

namespace App\Http\Controllers;

use App\Http\Requests\CoinRequest;
use App\Models\Coin;
use AshAllenDesign\LaravelExchangeRates\Rules\ValidCurrency;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class CoinController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->authorize('viewAny', Coin::class);
        $coins = Coin::all();
        if (empty($coins)) {
            return response()->json(null, 204);
        }
        return response()->json($coins, 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CoinRequest $request)
    {
        $this->authorize('create', Coin::class);

        $formData = [
            'currency' => $request->name,
        ];
        $rules = [
            'currency' => new ValidCurrency,
        ];
        $validator = Validator::make($formData, $rules);
        if (!$validator->fails()) {
            $coin = new Coin();
            $coin->fill($request->all());
            if ($this->isExists($coin) == false) {
                $coin->save();
                return response()->json($coin, 201);
            } else {
                return response()->json(['message' => 'Não autorizado, a moeda já existe'], 403);
            }
        } else {
            return response()->json(['message' => 'Não autorizado, a moeda não existe'], 403);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Coin  $coin
     * @return \Illuminate\Http\Response
     */
    public function show(Coin $coin)
    {
        $this->authorize('view', $coin);
        if (empty($coin)) {
            return response()->json(null, 404);
        }
        return response()->json($coin, 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Coin  $coin
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Coin $coin)
    {
        $this->authorize('update', $coin);
        if (empty($coin)) {
            return response()->json(null, 404);
        }
        $coin->fill($request->all());
        return response()->json($coin, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Coin  $coin
     * @return \Illuminate\Http\Response
     */
    public function destroy(Coin $coin)
    {
        $this->authorize('delete', $coin);
        if (empty($coin->name)) {
            return response()->json(null, 404);
        }
        $coin->delete();
        return response()->json(null, 204);
    }

    private function isExists($coin)
    {
        foreach (Coin::all() as $item) {
            if (Str::slug($coin->name) == Str::slug($item->name)) {
                return true;
                break;
            }
        }
        return false;
    }
}
