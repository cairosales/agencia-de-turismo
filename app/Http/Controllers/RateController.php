<?php

namespace App\Http\Controllers;

use App\Http\Requests\PriceRequest;
use App\Models\Coin;
use App\Models\Hotel;
use App\Models\Rate;
use App\Models\Room;
use AshAllenDesign\LaravelExchangeRates\Classes\ExchangeRate;
use Carbon\Carbon;
use Illuminate\Http\Request;

class RateController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->authorize('viewAny', Rate::class);
        $rates = Rate::with('hotel', 'room', 'coin')->get();
        if (empty($rates)) {
            return response()->json(null, 204);
        }
        return response()->json($rates, 200);
    }

    /**
     * Search itens the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function price(PriceRequest $request)
    {
        $this->authorize('view', Rate::class);
        $room = Room::find($request->room);
        $hotel = Hotel::find($request->hotel);
        $percentage = $request->percentage;
        $coin = Coin::find($request->coin);


        //Busca tradicional
        $rate = Rate::where('coin', $coin->id)->where('hotel', $hotel->id)->where('room', $room->id)->with('hotel', 'room', 'coin')->first();
        $exchange = false;
        if (empty($rate)) {
            $exchange = true;
            $rate = Rate::where('hotel', $hotel->id)->where('room', $room->id)->with('hotel', 'room', 'coin')->first();
            if (empty($rate)) {
                $error = "Cotação não disponível";
                return response()->json($error, 403);
            }
        }

        $value = $rate->value;
        $message = "Câmbio não aplicado";
        if ($exchange == true) {
            $message = "Câmbio aplicado para a moeda " . Coin::find($coin->id)->name;
            $exchangeRates = new ExchangeRate();
            $result = $exchangeRates->convert($value, Coin::find($coin->id)->name, 'BRL', Carbon::now());
            $value =  $result;
        }


        $valuePorcentage =  (($percentage * $value) / 100);
        $data = collect();
        $data->push(
            [
                'message' => $message,
                'porcentage' => $percentage,
                'profit margin' => $valuePorcentage,
                'value' => $value,
                'value_client' => ($value + $valuePorcentage)
            ]
        );
        $rate->data = $data->values();
        return response()->json($rate, 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->authorize('create', Rate::class);
        $rate = new Rate();
        $rate->fill($request->all());
        $rate->save();
        return response()->json($rate, 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Rate  $rate
     * @return \Illuminate\Http\Response
     */
    public function show(Rate $rate)
    {

        $this->authorize('view', $rate);
        return response()->json($rate, 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Rate  $rate
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Rate $rate)
    {
        $this->authorize('update', $rate);
        $rate->fill($request->all());
        $rate->save();
        return response()->json($rate, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Rate  $rate
     * @return \Illuminate\Http\Response
     */
    public function destroy(Rate $rate)
    {
        $this->authorize('delete', $rate);
        $rate->delete();
        return response()->json(null, 204);
    }
}
