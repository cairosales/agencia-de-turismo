<?php

namespace App\Http\Requests;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;

class RegisterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'=>['required','min:3','max:100'],
            'email'=>['required','email','unique:users'],
            'password'=>['required','min:8', 'confirmed'],
            'phone'=>['required','unique:users','min:8','max:11'],
            'type'=>['required','in:ADMIN,CLIENTE,REVENDEDOR,AGENCIA']
        ];
    }

    public function failedValidation(Validator $validator){
        throw new HttpResponseException(response()->json(['message'=>$validator->errors()->first()],422));
    }
}
