<?php

namespace App\Http\Requests;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;

class RateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'value' => ['required','regex:/^[0-9]+(\.[0-9][0-9]?)?$/'],
            'active' => ['required','boolean'],
            'hotel'=> ['required','exists:App\Models\Hotel,id'],
            'coin'=> ['required','exists:App\Models\Coin,id'],
            'room'=> ['required','exists:App\Models\Room,id']
        ];
    }

    public function failedValidation(Validator $validator){
        throw new HttpResponseException(response()->json(['error'=>$validator->errors()->first()],422));
    }
}
