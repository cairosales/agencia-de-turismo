<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Exceptions\TokenExpiredException;
use Tymon\JWTAuth\Exceptions\TokenInvalidException;
use Tymon\JWTAuth\Facades\JWTAuth;

class JWT
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $request->headers->set('Accept', 'application/json');
        try {
            //Access token from the request        
            $token =  JWTAuth::parseToken();
            //Try authenticating user       
            $user = $token->authenticate();
        } catch (TokenExpiredException $e) {
            //Thrown if token has expired        
            return $this->unauthorized('A chave de sessão expirou.  Por favor, faça login novamente.');
        } catch (TokenInvalidException $e) {
            //Thrown if token invalid
            return $this->unauthorized('A chave de sessão é inválida. Por favor, faça login novamente.');
        } catch (JWTException $e) {
            //Thrown if token was not found in the request.
            return $this->unauthorized('Chave de sessão não encontrada, é necessário incluir a chave no escopo de sessão');
        } 
        return $next($request);
    }

    private function unauthorized($message = null)
    {
        return response()->json([
            'message' => $message ? $message : 'Você não está autorizado a acessar esse recurso'
        ], 401);
    }
}
