<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class AdminUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = new User();
        $user->name = 'Cairo Sales';
        $user->email = 'cairosalesoliver@gmail.com';
        $user->phone = '88999177522';
        $user->password = Hash::make('asdfasdf');
        $user->type = 'ADMIN';
        $user->active = true;
        $user->save();
    }
}
