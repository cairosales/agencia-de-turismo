<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('login', [App\Http\Controllers\AuthController::class, 'login'])->name('login');
Route::post('register', [App\Http\Controllers\UserController::class, 'register'])->name('register');
Route::group(['prefix' => 'admin', 'middleware' => ['api','auth.jwt']], function ($router) {
    Route::apiResource('user', App\Http\Controllers\UserController::class)->names('admin.user');
    Route::apiResource('hotel', \App\Http\Controllers\HotelController::class)->names('admin.hotel');
    Route::apiResource('coin', \App\Http\Controllers\CoinController::class)->names('admin.coin');
    Route::apiResource('room', \App\Http\Controllers\RoomController::class)->names('admin.room');
    Route::post('rate/price',[\App\Http\Controllers\RateController::class,'price'])->name('admin.rate.price');
    Route::apiResource('rate', \App\Http\Controllers\RateController::class)->names('admin.rate');
    Route::post('logout', [App\Http\Controllers\AuthController::class, 'logout'])->name('logout');
    Route::post('refresh', [App\Http\Controllers\AuthController::class, 'refresh'])->name('refresh');
    Route::post('me', [App\Http\Controllers\AuthController::class, 'me'])->name('me');
});
