<?php

namespace Tests\Unit;

use App\Models\User;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Hash;
use Tests\TestCase;

class UserTest extends TestCase
{
    use DatabaseTransactions;
    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function test_check_user_admin_if_exists()
    {
        $this->assertDatabaseHas('users', [
            'type' => 'ADMIN'
        ]);
    }

    public function test_create_user(){
        $user = new User();
        $user->name = "Marcos Silva";
        $user->email = "marcoss_silva@gmail.com";
        $user->phone = "88999479909";
        $user->password = Hash::make("asdfasdf");
        $user->type = 'CLIENTE';
        $user->active = true;
        $user->save();
        $this->assertDatabaseHas('users',['email'=>$user->email]);
    }
}
